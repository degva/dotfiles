local lazy = require('lazy')

vim.g.mapleader = ","

require("lazy").setup({
	
  'tpope/vim-vinegar',
  -- 'vim-airline/vim-airline',
  -- 'vim-airline/vim-airline-themes',
  'rebelot/kanagawa.nvim',
  {
    'rebelot/heirline.nvim',
    after = {'kanagawa.nvim', 'nvim-web-devicons'}
  },
  {
    "yetone/avante.nvim",
    event = "VeryLazy",
    lazy = false,
    version = false, -- set this if you want to always pull the latest change
    opts = {
      provider = "claude", 
      auto_suggestions_provider = "claude",
    },
    -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
    build = "make",
    -- build = "powershell -ExecutionPolicy Bypass -File Build.ps1 -BuildFromSource false" -- for windows
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "stevearc/dressing.nvim",
      "nvim-lua/plenary.nvim",
      "MunifTanjim/nui.nvim",
      {
        -- Make sure to set this up properly if you have lazy=true
        'MeanderingProgrammer/render-markdown.nvim',
        opts = {
          file_types = { "markdown", "Avante" },
        },
        ft = { "markdown", "Avante" },
      },
    },
  },

  -- 'morhetz/gruvbox',
  -- 'edkolev/tmuxline.vim',
  'nvim-tree/nvim-web-devicons',
  'nvim-tree/nvim-tree.lua',
  {
    'neoclide/coc.nvim',
    branch = 'release'
  },
  {
    "ibhagwan/fzf-lua",
    -- optional for icon support
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      -- calling `setup` is optional for customization
      require("fzf-lua").setup({})
    end
  },
  'mattn/emmet-vim',
  'mlaursen/vim-react-snippets',
  -- 'jiangmiao/auto-pairs',
  -- 'github/copilot.vim',
  {
    'nvim-treesitter/nvim-treesitter', 
    cmd = 'TSUpdate'
  },
  {
    'akinsho/bufferline.nvim', 
    version = "*"
    -- dependencies = 'nvim-tree/nvim-web-devicons'
  },
  'lewis6991/gitsigns.nvim',
  'tpope/vim-fugitive',
  })
