vim.cmd [[
  augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
  augroup END
]]

vim.cmd [[autocmd BufNewFile,BufRead *.tsx set filetype=typescript.tsx]]
vim.cmd [[autocmd BufNewFile,BufRead *.jsx set filetype=javascript.jsx]]


-- Highlight the symbol and its references when holding the cursor.
vim.cmd [[
  autocmd CursorHold * silent call CocActionAsync('highlight')
]]

vim.cmd("syntax enable")

