local keyset = vim.keymap.set

-- Nvim tree
-- keyset('n', '<C-n>', ':NvimTreeToggle<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<C-n>", ":NvimTreeToggle<cr>", {silent = true, noremap = true})
keyset('n', '<leader>r', ':NvimTreeRefresh<CR>', { noremap = true, silent = true })

-- FZF
keyset('n', '<C-\\>', ':FzfLua buffers<CR>', { noremap = true, silent = true })
keyset('n', '<C-p>', ':FzfLua files<CR>', { noremap = true, silent = true })
keyset('n', '<C-g>', ':FzfLua live_grep<CR>', { noremap = true, silent = true })

-- COC
local keyset = vim.keymap.set
function _G.check_back_space()
    local col = vim.fn.col('.') - 1
    return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
end
-- Use Tab for trigger completion with characters ahead and navigate
-- NOTE: There's always a completion item selected by default, you may want to enable
-- no select by setting `"suggest.noselect": true` in your configuration file
-- NOTE: Use command ':verbose imap <tab>' to make sure Tab is not mapped by
-- other plugins before putting this into your config
local opts = {silent = true, noremap = true, expr = true, replace_keycodes = false}
keyset("i", "<TAB>", 'coc#pum#visible() ? coc#pum#next(1) : v:lua.check_back_space() ? "<TAB>" : coc#refresh()', opts)
keyset("i", "<S-TAB>", [[coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"]], opts)

-- Make <CR> to accept selected completion item or notify coc.nvim to format
-- <C-g>u breaks current undo, please make your own choice
keyset("i", "<cr>", [[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]], opts)

-- Use <c-j> to trigger snippets
keyset("i", "<c-j>", "<Plug>(coc-snippets-expand-jump)")
-- Use <c-space> to trigger completion
keyset("i", "<c-space>", "coc#refresh()", {silent = true, expr = true})
keyset('n', 'gd', '<Plug>(coc-definition)', { silent = true })
keyset('n', 'gy', '<Plug>(coc-type-definition)', { silent = true })
keyset('n', 'gi', '<Plug>(coc-implementation)', { silent = true })
keyset('n', 'gr', '<Plug>(coc-references)', { silent = true })

-- Remap <C-f> and <C-b> for scroll float windows/popups.
local coc_scroll_mapping = function(key, mode)
  if vim.fn['coc#float#has_scroll']() == 1 then
    if mode == 'n' then
      return vim.fn['coc#float#scroll'](key == '<C-f>' and 1 or 0)
    else -- mode is 'i'
      return string.format("<c-r>=coc#float#scroll(%d)<cr>", key == '<C-f>' and 1 or 0)
    end
  else
    if mode == 'n' then
      return key
    else -- mode is 'i'
      return key == '<C-f>' and "<Right>" or "<Left>"
    end
  end
end

local opts = {silent = true, nowait = true, expr = true}
keyset("n", "<C-f>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', opts)
keyset("n", "<C-b>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', opts)
keyset("i", "<C-f>",
       'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(1)<cr>" : "<Right>"', opts)
keyset("i", "<C-b>",
       'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(0)<cr>" : "<Left>"', opts)
keyset("v", "<C-f>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', opts)
keyset("v", "<C-b>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', opts)

-- Use K to show documentation in preview window
function _G.show_docs()
    local cw = vim.fn.expand('<cword>')
    if vim.fn.index({'vim', 'help'}, vim.bo.filetype) >= 0 then
        vim.api.nvim_command('h ' .. cw)
    elseif vim.api.nvim_eval('coc#rpc#ready()') then
        vim.fn.CocActionAsync('doHover')
    else
        vim.api.nvim_command('!' .. vim.o.keywordprg .. ' ' .. cw)
    end
end
keyset("n", "K", '<CMD>lua _G.show_docs()<CR>', {silent = true})



-- Symbol renaming, <leader> is ,
keyset("n", "<leader>rn", "<Plug>(coc-rename)", {silent = true})

-- Search and replace under cursor <leader> is ,
keyset('n', '<leader>s', ':%s/\\<<C-r><C-w>\\>/', { silent = true })

-- disable arrow keys in insert mode
keyset('i', '<up>', '<nop>', { silent = true })
keyset('i', '<down>', '<nop>', { silent = true })
keyset('i', '<left>', '<nop>', { silent = true })
keyset('i', '<right>', '<nop>', { silent = true })

-- disable arrow keys in normal mode 
keyset('n', '<up>', '<nop>', { silent = true })
keyset('n', '<down>', '<nop>', { silent = true })
keyset('n', '<left>', '<nop>', { silent = true })
keyset('n', '<right>', '<nop>', { silent = true })

-- For git blame
keyset('n', '<leader>Gb', ':Git blame<CR>', { silent = true })
keyset('n', '<leader>Gvd', ':Gvdiffsplit<CR>', { silent = true })
keyset('n', '<leader>Gd', ':Gdiffsplit<CR>', { silent = true })

